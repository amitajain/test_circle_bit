FROM python:2.7-slim

RUN mkdir -p /usr/src
WORKDIR /usr/src

COPY requirement.txt ./

RUN pip install --no-cache-dir -r requirement.txt

COPY . /usr/src

CMD cd src && python main.py