import psycopg2
conn = psycopg2.connect(dbname='postgres', user='postgres', host='localhost',port=6432,password='')
cur = conn.cursor()
cur.execute("create table users (id varchar(255), name varchar(255));")
f = open(r'test.csv', 'r')
cur.copy_from(f,'users', sep=',')
f.close()
cur.execute("select * from users;")
print cur.fetchall()
conn.close();
print "I am connected to the database"
